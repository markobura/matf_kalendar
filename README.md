# About

A project for the university course "Programski jezik Javascript" held by Levi9 at MATF, year 2021/2022. A simple calendar application.

More info at https://github.com/levinine/javascript-matf-2019

# Instructions

1. Clone repository

``` 
git clone https://gitlab.com/markobura/matf_kalendar.git
```

2. Start backend (server) - from directory **backend** do:

``` 
npm install
```

``` 
nodemon server.js
```

3. Start frontend - from directory **frontend** do:

``` 
npm install
```

``` 
nodemon run dev
```

4. App is active on https://localhost:3000






