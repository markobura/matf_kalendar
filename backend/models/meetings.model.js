const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const meetingSchema = new Schema(
  {
    title: { type: String, required: true },
    day: {type: Number, required: true},
    time: { type: String, required: true },
    description: { type: String, required: true },
    participants: { type: [String], required: true },
  },
  {
    timestamps: true,
  }
);

const Meeting = mongoose.model("Meetings", meetingSchema);

module.exports = Meeting;
