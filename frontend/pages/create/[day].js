import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import Meeting from "../../src/components/Meeting";


const CreateMeeting = () => {
  const router = useRouter();
  const {day}  = router.query;

  return (
    <div>
        <Meeting date={day}/>
    </div>
  );
}

export default CreateMeeting;
