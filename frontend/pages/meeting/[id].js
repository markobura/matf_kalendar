import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import styles from "./meeting.module.css";
import Meeting from "../../src/components/Meeting";


const Id = () => {
    const router = useRouter();
    const { id } = router.query;

    const [meeting, setMeeting] = useState([]);

    useEffect(() => {
        fetch("http://localhost:5000/meetings/" + id)
          .then((res) => res.json())
          .then((data) => setMeeting(data));
      }, []);

    const deleteMeeting = () => {
        const options = {
          method: "DELETE",
          headers: {"Content-Type" : "application/json"},
          query: JSON.stringify(id)
        }
    
        fetch("http://localhost:5000/meetings/" + id, options)
        .then(router.back());
      }

    return (
        <div className={styles.details}>
            <h2 className={styles.h2}>{meeting?.title}</h2>
            <p>Desctiption: {meeting?.description}</p>
            <p>Time: {meeting?.time}</p>
            {meeting.participants?.length > 0 ? <h3 className={styles.h3}>Participants: </h3> : <></>}
            <ul>
            {meeting.participants?.map((p, i) => (
                <li key={i}>{p}</li>
            ))}
            </ul>
            <button className={styles.button} onClick={() => deleteMeeting()}>Delete meeting</button>
            <button className={styles.button} onClick={() => router.push("/")}>Back</button>

        </div>
    )
}

export default Id;