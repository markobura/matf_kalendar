import { useRouter } from "next/router";
import styles from "./successful.module.css";

const Successful = () => {
    const router = useRouter();
    return (
        <div>
            <h2 className={styles.h2}>Uspesno ste dodali sastanak</h2>
            <button className={styles.button} onClick={() => {router.back()}}>Back</button>
        </div>
    );
}

export default Successful;
