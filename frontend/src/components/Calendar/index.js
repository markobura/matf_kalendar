import { useState, useEffect } from "react";
import Day from "../Day";
import Modal from "../Modal";
import Meeting from "../Meeting";
import styles from "../../../styles/Home.module.css";

const Calendar = (allMeetings) => {

  const month = [
    ["01", "02", "03", "04", "05", "06", "07"],
    ["08", "09", "10", "11", "12", "13", "14"],
    ["15", "16", "17", "18", "19", "20", "21"],
    ["22", "23", "24", "25", "26", "27", "28"],
  ];

  const [meetings, setMeetings] = useState([]);
  useEffect(() => {
    fetch("http://localhost:5000/meetings")
      .then((res) => res.json())
      .then((data) => setMeetings(data))
  }, []);

  const [newMeeting, setNewMeeting] = useState([]);
  const [showModal, setShowModal] = useState(false);
 
  return (
    <div className={styles.main}>
        <h1 className={styles.myHeaderStyle}>February</h1>
        <table>
        <thead>
        <tr>
            <td className={styles.title}>MONDAY</td>
            <td className={styles.title}>TUESDAY</td>
            <td className={styles.title}>WEDNESDAY</td>
            <td className={styles.title}>THURSDAY</td>
            <td className={styles.title}>FRIDAY</td>
            <td className={styles.title}>SATURDAY</td>
            <td className={styles.title}>SUNDAY</td>
        </tr>
        </thead>
        <tbody>
            {month.map((week, index) => (
                    <tr key={index}>
                    {week.map((day, index) => (
                        <td key={index}>
                        <Day
                            day={day}
                            meetings={meetings}
                            modal={() => {
                              setShowModal(true);
                              setNewMeeting(day);
                            }}
                        ></Day>
                        </td>
                    ))}
                    </tr>
                ))}
        </tbody>
        </table>
        <Modal showModal={showModal}>
        <Meeting
            close={() => {setShowModal(false);}}
            date={newMeeting}
        ></Meeting>
        </Modal>
    </div>
  )

}

export default Calendar;