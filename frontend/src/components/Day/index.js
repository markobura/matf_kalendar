import { useRouter } from "next/router";
import styles from "./day.module.css";

const Day = ({day, meetings, modal}) => {
    
    const router = useRouter();
    
    const meetingsPerDay = meetings.filter((x) => Number(x.day) == Number(day));

    return (
        <div className={styles.day} onDoubleClick={modal}>
            <h1>{day}</h1>
            {
                meetingsPerDay.map((d, idx) => {
                    return <p  key={idx} className={styles.event}
                                onClick={() => {router.push('/meeting/' + d._id)}}>
                                    {d.title} : {d.time}
                            </p>
                })
            }
        </div>
    );
};

export default Day;