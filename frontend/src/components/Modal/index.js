import styles from "./modal.module.css";

const Modal = ({ showModal, children }) => {
    const show = showModal ? styles.display : styles.hide;

    return (
        <div className={show}>
            <section className={styles.main}>{children}</section>
        </div>
    );
};

export default Modal;