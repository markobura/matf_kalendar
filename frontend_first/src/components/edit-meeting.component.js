import React, { Component }  from "react";
import axios from "axios";

export default class EditMeeting extends Component {
    constructor(props) {
        super(props);
    
        this.onChangeTitle = this.onChangeTitle.bind(this);
        this.onChangeDay = this.onChangeDay.bind(this);
        this.onChangeTime = this.onChangeTime.bind(this);
        this.onChangeDescription = this.onChangeDescription.bind(this);
        this.onChangeParticipants = this.onChangeParticipants.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    
        this.state = {
          title: '',
          day: 0,
          time: '',
          description: '',
          participants: [],
          users: []
        }
    }
    
    componentDidMount() {
        axios.get('http://localhost:5000/meetings/'+this.props.match.params.id)
            .then(res => {
                this.setState({
                    title: res.data.title,
                    day: res.data.day,
                    time: res.data.time,
                    description: res.data.description,
                    participants: res.data.participants[0]
                })
            })
            .catch(function(error) {
                console.log(error);
            })        
        axios.get('http://localhost:5000/users/')
            .then(res => {
                if (res.data.length > 0) {
                    this.setState({
                        users: res.data.map(user => user.username),

                    })
                }
            })
    }

    onChangeTitle(e) {
        this.setState({
            title: e.target.value
        });
    }

    onChangeDay(e) {
        this.setState({
            day: e.target.value
        });
    }
    
    onChangeTime(e) {
        this.setState({
            time: e.target.value
        });
    }

    onChangeDescription(e) {
        this.setState({
            description: e.target.value
        });
    }

    onChangeParticipants(e) {
        this.setState({
            participants: e.target.value
        });
    }

    onSubmit(e) {
        e.preventDefault();

        const meeting = {
            title: this.state.title,
            day: this.state.day,
            time: this.state.time,
            description: this.state.description,
            participants: this.state.participants
        }

        console.log(meeting);

        axios.post('http://localhost:5000/meetings/update/'+this.props.match.params.id, meeting)
            .then(res => console.log(res.data));

        window.location = '/';
    }

    render() {
        return (
        <div>
          <h3>Edit New Meeting</h3>
          <form onSubmit={this.onSubmit}>
            <div className="form-group">
                <label>Participant</label>
                <select ref="userInput"
                    required
                    className="form-control"
                    value={this.state.participants}
                    onChange={this.onChangeParticipants}>
                        {
                            this.state.users.map(function(user) {
                                return <option
                                    key={user}
                                    value={user}>{user}
                                        </option>;
                            })
                        }
                </select>
            </div>
            <div className="form-group"> 
              <label>Title: </label>
              <input  type="text"
                  required
                  className="form-control"
                  value={this.state.title}
                  onChange={this.onChangeTitle}
                  />
            </div>
            <div className="form-group">
              <label>Day: </label>
              <input 
                  type="text" 
                  className="form-control"
                  value={this.state.day}
                  onChange={this.onChangeDay}
                  />
            </div>
            <div className="form-group"> 
              <label>Description: </label>
              <input  type="text"
                  required
                  className="form-control"
                  value={this.state.description}
                  onChange={this.onChangeDescription}
                  />
            </div>
            <div className="form-group"> 
              <label>Time: </label>
              <input  type="text"
                  required
                  className="form-control"
                  value={this.state.time}
                  onChange={this.onChangeTime}
                  />
            </div>
    
            <div className="form-group">
              <input type="submit" value="Edit Meeting" className="btn btn-primary" />
            </div>
          </form>
        </div>
        )
      }
}