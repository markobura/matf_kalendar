import React, { Component }  from "react";
import { Link } from 'react-router-dom';
import axios from "axios";

const Meeting = props => (
    <tr>
        <td>{props.meeting.participants}</td>
        <td>{props.meeting.title}</td>
        <td>{props.meeting.day}</td>
        <td>{props.meeting.time}</td>
        <td>{props.meeting.description}</td>
        <td>
            <Link to={"/edit/"+props.meeting._id}>edit</Link> | <a href="#" onClick={() => {props.deleteMeeting(props.meeting._id)}}>delete</a>
        </td>
    </tr>
    
    
)

export default class MeetingsList extends Component {
    constructor(props) {
        super(props);

        this.deleteMeeting = this.deleteMeeting.bind(this);

        this.state = {
            meetings: []
        };
    }

    componentDidMount() {
        axios.get('http://localhost:5000/meetings/')
            .then(res => {
                this.setState({ meetings: res.data})
            })
            .catch((error) => {
                console.log(error);
            })
    }

    deleteMeeting(id) {
        axios.delete('http://localhost:5000/meetings/' + id)
            .then(res => console.log(res.data));

        this.setState({
            meetings: this.state.meetings.filter(el => el._id !== id)
        })
    }

    meetingsList() {
        return this.state.meetings.map(currentMeeting => {
            return <Meeting meeting={currentMeeting} deleteMeeting={this.deleteMeeting} key={currentMeeting._id} />
        })
    }


    
    render() {
        return (
            <div>
                <h3>Meetings</h3>
                <table className="table">
                    <thead className="thead-light">
                        <tr>
                            <th>Participants</th>
                            <th>Title</th>
                            <th>Day</th>
                            <th>Time</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.meetingsList()}
                    </tbody>
                </table>
            </div>
        )
    }
}
